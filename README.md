# NGuild 年公会插件

#### 介绍
NGuild拥有排行榜、贡献点、成员管理等多个功能。
如果NGuild产生了异常，请及时在issue中提交详细信息，包括但不限服务器信息、前置插件信息、本插件信息、配置文件信息。

#### 联系我
| 方式   |                 |
|------|-----------------|
| Mail | mcayear@163.com |
| QQGroup   | 1022801913    |
| MineBBS   | [link](https://www.minebbs.com/threads/nguild.5185/)    |